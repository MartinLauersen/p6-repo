#!/usr/bin/env python
import rospy
from gazebo_msgs.msg import LinkState

def talker():
    pub = rospy.Publisher('/gazebo/set_link_state', LinkState)
    rospy.init_node('gazebo_interface', anonymous=True)
    r = rospy.Rate(100) 

    # Create msg and specify that it refers to drone
    msg = LinkState()
    msg.link_name = 'drone_link'

    # Position
    msg.pose.position.x = 1.0
    msg.pose.position.y = 1.0
    msg.pose.position.z = 10.0

    # Orientation



    while not rospy.is_shutdown():
        rospy.loginfo(msg)
        pub.publish(msg)
        r.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException: pass
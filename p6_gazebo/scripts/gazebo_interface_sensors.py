#!/usr/bin/env python2
from __future__ import print_function
import sys


import rospy
from gazebo_msgs.msg import LinkState

from geometry_msgs.msg import TwistStamped
from geometry_msgs.msg import Twist

from tf.transformations import quaternion_from_euler

class GazeboInterface():
         
    def __init__(self):


        self.velocity = Twist()
        self.position = Twist()
        self.last_time = None
        self.dt = 0


        
        # 
        self.link_state_pub = rospy.Publisher('/gazebo/set_link_state', LinkState, queue_size=1)
        self.drone_states_position_pub = rospy.Publisher('/drone/states/position', TwistStamped, queue_size=1)
        self.drone_states_velocity_pub = rospy.Publisher('/drone/states/velocity', TwistStamped, queue_size=1)

        # 
        self.twist_stamped_sub = rospy.Subscriber("/drone/controller/output", TwistStamped, self.newSetpointCallback)


    def newSetpointCallback(self, data):

        if self.last_time == None:
            self.last_time = rospy.Time.now()
            return

        else:
            time_now = rospy.Time.now()
            self.dt = time_now - self.last_time
            self.last_time = time_now
            # 
            if data.header.frame_id == 'velocity':
                self.velToPosition(data)
            else:
                self.forcesToPosition(data)


            # Publish the results
            msg = LinkState()
            msg.link_name = 'drone_link'

            # Position
            msg.pose.position.x = self.position.linear.x
            msg.pose.position.y = self.position.linear.y
            msg.pose.position.z = self.position.linear.z

            # Convert roll,pitch and yaw to quaternion
            q = quaternion_from_euler(self.position.angular.x,
                                self.position.angular.y, self.position.angular.z)

            msg.pose.orientation.x = q[0]
            msg.pose.orientation.y = q[1]
            msg.pose.orientation.z = q[2]
            msg.pose.orientation.w = q[3]

            self.link_state_pub.publish(msg)


            # Publish current position.
            msg = TwistStamped()
            msg.header.frame_id = 'drone'
            msg.header.stamp = rospy.Time.now()

            msg.twist = self.position
            self.drone_states_position_pub.publish(msg)

            # Publish current velocity
            msg = TwistStamped()
            msg.header.frame_id = 'drone'
            msg.header.stamp = rospy.Time.now()

            msg.twist = self.velocity
            self.drone_states_velocity_pub.publish(msg)           





    def forcesToPosition(self, data):

        # Forces on x and y direction should be 0 for a drone
        data.twist.linear.x = 0.0
        data.twist.linear.y = 0.0

        # Upward thrust
        data.twist.linear.z = 0.0 

        # Torques
        data.twist.angular.x = 0.0
        data.twist.angular.y = 0.0
        data.twist.angular.z = 0.0



        print("Not implemented")


    def velToPosition(self, data):

        
        dt_sec = self.dt.nsecs*0.000000001
        print(dt_sec)

        # Increment the position
        self.position.linear.x += data.twist.linear.x * dt_sec
        self.position.linear.y += data.twist.linear.y * dt_sec
        self.position.linear.z += data.twist.linear.z * dt_sec

        # Increment the orientation
        self.position.angular.x += data.twist.angular.x * dt_sec
        self.position.angular.y += data.twist.angular.y * dt_sec
        self.position.angular.z += data.twist.angular.z * dt_sec


def main(args):
    gazebo_interface_node = GazeboInterface()
    rospy.init_node('gazebo_interface_sensor_node', anonymous=False)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")

if __name__ == '__main__':
    main(sys.argv)
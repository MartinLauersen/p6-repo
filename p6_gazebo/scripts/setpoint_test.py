#!/usr/bin/env python
import rospy
from geometry_msgs.msg import TwistStamped


def talker():
    pub = rospy.Publisher('/setpoints', TwistStamped)
    rospy.init_node('setpoint_publisher', anonymous=False)
    r = rospy.Rate(100) 

    # Create msg and specify that it refers to drone
    msg = TwistStamped()
    msg.header.frame_id = 'velocity'
    msg.header.stamp = rospy.Time.now()

    # Position
    msg.twist.linear.x = 0
    msg.twist.linear.y = 0
    msg.twist.linear.z = 0.1

    # Orientation
    msg.twist.angular.x = 0.1
    msg.twist.angular.y = 0
    msg.twist.angular.z = 0


    while not rospy.is_shutdown():
        rospy.loginfo(msg)
        pub.publish(msg)
        r.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException: pass


class PID():

    def __init__(self, kp, ki, kd):

        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.kn = kn
        self.sat_min = sat_min
        self.sat_max = sat_max

        self.derivative_tau = derivative_tau

        self.time_prev = 0

        self.integral = 0
        self.error_prev = 0
        self.derivative_error_prev = 0

    def update(self, setpoint, measure):

        # Figure out dt
        time_now = rospy.Time.now()
        dt = time_now - self.time_prev
        self.time_prev = time_now
        dt = dt.nsec * 0.000000001

        error = setpoint - measure

        integral_error = self.ki * error * dt

        derivative_error = (self.kd * (error - error_prev) + self.derivative_tau * derivative_error_prev) / (self.derivative_tau + dt)

        output = self.kp * error + self.integral + integral_error + derivative_error


        # update variables
        self.error_prev = error
        self.derivative_error_prev = derivative_error

        # Saturate output
        if output > self.sat_max:
            output = self.sat_max
            if integral_error < 0.0:
                self.integral += integral_error

        elif output < self.sat_min:
            output = self.sat_min
            if integral_error > 0.0:
                self.integral += integral_error

        else:
            self.integral += integral_error

        return output









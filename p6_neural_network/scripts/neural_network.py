#!/usr/bin/env python2
#from __future__ import print_function

import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import time
import os

from p6_neural_network.msg import nnOutput


import rospkg
rp = rospkg.RosPack()
path = os.path.join(rp.get_path("p6_neural_network"), "scripts")

weights_path = labels_file = os.path.join(path,"SSD_MODEL.h5")

CLASS_NAMES = ['background','windturbine', 'wings', 'housing', 'hub','shaft']


from keras.optimizers import Adam, SGD
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, TerminateOnNaN, CSVLogger
from keras import backend as K
from keras.models import load_model
from math import ceil
import numpy as np

from models.keras_ssd300 import ssd_300
from keras_loss_function.keras_ssd_loss import SSDLoss
from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_DecodeDetectionsFast import DecodeDetectionsFast
from keras_layers.keras_layer_L2Normalization import L2Normalization

from ssd_encoder_decoder.ssd_input_encoder import SSDInputEncoder
from ssd_encoder_decoder.ssd_output_decoder import decode_detections, decode_detections_fast

from data_generator.object_detection_2d_data_generator import DataGenerator
from data_generator.object_detection_2d_geometric_ops import Resize
from data_generator.object_detection_2d_photometric_ops import ConvertTo3Channels
from data_generator.data_augmentation_chain_original_ssd import SSDDataAugmentation
from data_generator.object_detection_2d_misc_utils import apply_inverse_transforms


img_height = 300 # Height of the model input images
img_width = 300 # Width of the model input images
img_channels = 3 # Number of color channels of the model input images
mean_color = [123, 117, 104] # The per-channel mean of the images in the dataset. Do not change this value if you're using any of the pre-trained weights.
swap_channels = [2, 1, 0] # The color channel order in the original SSD is BGR, so we'll have the model reverse the color channel order of the input images.
n_classes = 5 # Number of positive classes, e.g. 20 for Pascal VOC, 80 for MS COCO
scales_pascal = [0.1, 0.2, 0.37, 0.54, 0.71, 0.88, 1.05] # The anchor box scaling factors used in the original SSD300 for the Pascal VOC datasets
scales_coco = [0.07, 0.15, 0.33, 0.51, 0.69, 0.87, 1.05] # The anchor box scaling factors used in the original SSD300 for the MS COCO datasets
scales = scales_pascal
aspect_ratios = [[1.0, 2.0, 0.5],
                 [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                 [1.0, 2.0, 0.5],
                 [1.0, 2.0, 0.5]] # The anchor box aspect ratios used in the original SSD300; the order matters
two_boxes_for_ar1 = True
steps = [8, 16, 32, 64, 100, 300] # The space between two adjacent anchor box center points for each predictor layer.
offsets = [0.5, 0.5, 0.5, 0.5, 0.5, 0.5] # The offsets of the first anchor box center points from the top and left borders of the image as a fraction of the step size for each predictor layer.
clip_boxes = False # Whether or not to clip the anchor boxes to lie entirely within the image boundaries
variances = [0.1, 0.1, 0.2, 0.2] # The variances by which the encoded target coordinates are divided as in the original implementation
normalize_coords = True




# 1: Build the Keras model.

K.clear_session() # Clear previous models from memory.

model = ssd_300(image_size=(img_height, img_width, img_channels),
                n_classes=n_classes,
                mode='training',
                l2_regularization=0.0005,
                scales=scales,
                aspect_ratios_per_layer=aspect_ratios,
                two_boxes_for_ar1=two_boxes_for_ar1,
                steps=steps,
                offsets=offsets,
                clip_boxes=clip_boxes,
                variances=variances,
                normalize_coords=normalize_coords,
                subtract_mean=mean_color,
                swap_channels=swap_channels)



# 3: Instantiate an optimizer and the SSD loss function and compile the model.
#    If you want to follow the original Caffe implementation, use the preset SGD
#    optimizer, otherwise I'd recommend the commented-out Adam optimizer.

adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
sgd = SGD(lr=0.001, momentum=0.9, decay=0.0, nesterov=False) 

ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)


# Freeze layers
for i in range(len(model.layers)):
    model.layers[i].trainable=False

model.compile(optimizer=adam, loss=ssd_loss.compute_loss)

model.load_weights(weights_path)
model._make_predict_function()




class image_converter:

  def __init__(self):


    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/neural_network/detection/input_image",Image,self.callback)

    self.nn_output_pub = rospy.Publisher("/neural_network/detection/output", nnOutput, queue_size=1)

    self.counter = 0

  def callback(self,data):
    print("got image")
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      cv_image = cv2.resize(cv_image,(300,300))
    except CvBridgeError as e:
      print(e)


    time_start = time.time()

  
    #
    #kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
    #cv_image = cv2.filter2D(cv_image, -1, kernel)

    # Convert to RGB
    image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
    

    # Expand dimension            print(p)
    batch = np.zeros((1,300,300,3))
    batch[0,:,:,:] = image

    ##############################################################
    # predict and find boxes
    y_pred = model.predict(batch)

    y_pred_decoded = decode_detections(y_pred,
                                   confidence_thresh=0.5,
                                   iou_threshold=0.5,
                                   top_k=200,
                                   normalize_coords=normalize_coords,
                                   img_height=img_height,
                                   img_width=img_width)


    """
    for pred in y_pred_decoded:
        for p in pred:
            # label, confidence, x start , y start, x end, y end
            image = cv2.rectangle(cv_image, (int(p[2]),int(p[3])), (int(p[4]),int(p[5])), (0,0,255), 1) 
            conf_str = "Confidence {:.2f}".format(p[1])
            cv2.putText(cv_image, conf_str, (int(p[2]), int(p[3])-10), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (36,255,12), 1)
            """

    msg = nnOutput()
    for pred in y_pred_decoded:
        print(pred)
        for p in pred:

            msg.class_name.append(CLASS_NAMES[int(p[0])])
            msg.confidence.append(p[1])
            msg.x_start.append(p[2])
            msg.y_start.append(p[3])
            msg.x_end.append(p[4])
            msg.y_end.append(p[5])

            if p[0] == 2.0:
              image = cv2.rectangle(cv_image, (int(p[2]),int(p[3])), (int(p[4]),int(p[5])), (0,0,255), 1) 
              #image = cv2.circle(cv_image, (int((p[2]+p[4])*0.5), int((p[3]+p[5])*0.5)), 5, (0,255,0), 2)
            if p[0] == 4.0:
              image = cv2.rectangle(cv_image, (int(p[2]),int(p[3])), (int(p[4]),int(p[5])), (0,255,0), 1) 
            if p[0] == 3.0:
              image = cv2.rectangle(cv_image, (int(p[2]),int(p[3])), (int(p[4]),int(p[5])), (255,0,0), 1) 
            if p[0] == 5.0:
              image = cv2.rectangle(cv_image, (int(p[2]),int(p[3])), (int(p[4]),int(p[5])), (255,0,255), 1) 
    self.nn_output_pub.publish(msg)

    print(time.time()-time_start)
    ##############################################################

    # show the output image
    cv2.imshow("Object detection", cv_image)
    cv2.waitKey(3)
    ############################################################


def main(args):
    print("started object_detection_node")
    ic = image_converter()
    rospy.init_node('object_detection_node', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
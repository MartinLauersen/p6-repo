#!/usr/bin/env python2
#from __future__ import print_function

import roslib
import sys
import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import time
import os



import tensorflow as tf
print(tf.__version__)

from keras.utils import plot_model
from keras.models import Model
from keras.layers import Input
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate

IMG_SIZE = 256


import rospkg
rp = rospkg.RosPack()
path = os.path.join(rp.get_path("p6_neural_network"), "scripts")

weights_path =  os.path.join(path,"weights_segmentation_multilabel2.h5")

############################################################################################
def downsample(filters = 32, kernel_size = 3, activation='leaky_relu',
                 batch_norm = False, dropout = None ,name=None):
    

    initializer = tf.random_normal_initializer(0.0, 0.02)

    layer = tf.keras.Sequential()
    layer.add(tf.keras.layers.Conv2D(filters, kernel_size,
                                     strides=2, padding='same',
                                     kernel_initializer=initializer))
    if activation == 'leaky_relu':
        layer.add(tf.keras.layers.LeakyReLU())
    else:
        layer.add(tf.keras.layers.ReLU())
    
    if batch_norm == True:
        layer.add(tf.keras.layers.BatchNormalization())
    
    if dropout != None:
        layer.add(tf.keras.layers.Dropout(dropout))
        
    layer_same = tf.keras.Sequential()
    layer_same.add(tf.keras.layers.Conv2D(filters, kernel_size,
                                     strides=1, padding='same',
                                     kernel_initializer=initializer))
    layer_same.add(tf.keras.layers.LeakyReLU())
    
    if batch_norm == True:
        layer_same.add(tf.keras.layers.BatchNormalization())
    if dropout != None:
        layer_same.add(tf.keras.layers.Dropout(dropout))
    
    return layer_same, layer
    

def upsample(filters = 32, kernel_size = 3, activation='leaky_relu',
            batch_norm = False, dropout = None ,name=None):
    
    
    initializer = tf.random_normal_initializer(0., 0.02)

    layer = tf.keras.Sequential()
    layer.add(tf.keras.layers.Conv2DTranspose(filters, kernel_size,
                                             strides=2, padding='same',
                                             kernel_initializer=initializer))
    if activation == 'leaky_relu':
        layer.add(tf.keras.layers.LeakyReLU())
        
    
    if batch_norm == True:
        layer.add(tf.keras.layers.BatchNormalization())
    
    if dropout != None:
        layer.add(tf.keras.layers.Dropout(dropout))
    
    return layer


def conv_layer(filters = 32, kernel_size = 3, activation='leaky_relu',
            batch_norm = False, dropout = None ,name=None):
    
    initializer = tf.random_normal_initializer(0., 0.02)

    layer = tf.keras.Sequential()
    layer.add(tf.keras.layers.Conv2DTranspose(filters, kernel_size,
                                             strides=1, padding='same',
                                             kernel_initializer=initializer))
    if activation == 'leaky_relu':
        layer.add(tf.keras.layers.LeakyReLU())
        
    if batch_norm == True:
        layer.add(tf.keras.layers.BatchNormalization())
    
    if dropout != None:
        layer.add(tf.keras.layers.Dropout(dropout))
    
    return layer

# Input size (image width, image height, number of channels)
INPUTS = tf.keras.Input(shape=(IMG_SIZE,IMG_SIZE,3))
#INPUTS._name = 'Input image'
### Down stack ###



conv128, down64 = downsample(filters = 64, kernel_size=3, batch_norm=False, dropout=None)
conv128_out = conv128(INPUTS)
down64_out = down64(conv128_out)

conv64, down32 = downsample(filters = 64, kernel_size=3, batch_norm=True, dropout=0.3)
conv64_out = conv64(down64_out)
down32_out = down32(conv64_out)

conv32, down16 = downsample(filters = 64, kernel_size=3, batch_norm=True, dropout=0.3)
conv32_out = conv32(down32_out)
down16_out = down16(conv32_out)
##
conv16, down8 = downsample(filters = 64, kernel_size=3, batch_norm=True, dropout=0.3)
conv16_out = conv16(down16_out)
down8_out = down8(conv16_out)

conv8, down4 = downsample(filters = 64, kernel_size=3, batch_norm=True, dropout=0.3)
conv8_out = conv8(down8_out)
down4_out = down4(conv8_out)

conv4, down2 = downsample(filters = 128, kernel_size=3, batch_norm=True, dropout=0.3)
conv4_out = conv4(down4_out)
down2_out = down2(conv4_out)

conv2, down1 = downsample(filters = 256, kernel_size=3, batch_norm=True, dropout=0.3)
conv2_out = conv2(down2_out)
down1_out = down1(conv2_out)


### Up stack ###

### 2x2 ###
# Layers
deconv2 = upsample(filters = 256, kernel_size = 3, batch_norm = True, dropout = 0.3)
conv2_up = conv_layer(filters = 256,kernel_size = 3)
# Operations
up2 = deconv2(down1_out)
merge2 = tf.keras.layers.concatenate([conv2_out, up2], axis = -1)
up2_out = conv2_up(merge2)

### 4x4 ###
deconv4 = upsample(filters = 128, kernel_size = 3, batch_norm = True, dropout = 0.3)
conv4_up = conv_layer(filters = 128,kernel_size = 3, batch_norm = True, dropout = 0.3)
# Operations
up4 = deconv4(up2_out)
merge4 = tf.keras.layers.concatenate([conv4_out, up4], axis = -1)
up4_out = conv4_up(merge4)

### 8x8 ###
deconv8 = upsample(filters = 64, kernel_size = 3, batch_norm = True, dropout = 0.3)
conv8_up = conv_layer(filters = 64,kernel_size = 3, batch_norm = True, dropout = 0.3)
# Operations
up8 = deconv8(up4_out)
merge8 = tf.keras.layers.concatenate([conv8_out, up8], axis = -1)
up8_out = conv8_up(merge8)

### 16x16 ###
deconv16 = upsample(filters = 64, kernel_size = 3, batch_norm = True, dropout = 0.3)
conv16_up = conv_layer(filters = 64,kernel_size = 3, batch_norm = True, dropout = 0.3)
# Operations
up16 = deconv16(up8_out)
merge16 = tf.keras.layers.concatenate([conv16_out, up16], axis = -1)
up16_out = conv16_up(merge16)

### 32x32 ###
deconv32 = upsample(filters = 64, kernel_size = 3, batch_norm = True, dropout = 0.3)
conv32_up = conv_layer(filters = 64,kernel_size = 3, batch_norm = True, dropout = 0.3)
# Operations
up32 = deconv32(up16_out)
merge32 = tf.keras.layers.concatenate([conv32_out, up32], axis = -1)
up32_out = conv32_up(merge32)

### 64x64 ###
deconv64 = upsample(filters = 64, kernel_size = 3, batch_norm = True, dropout = 0.3)
conv64_up = conv_layer(filters = 64,kernel_size = 3, batch_norm = True, dropout = 0.3)
# Operations
up64 = deconv64(up32_out)
merge64 = tf.keras.layers.concatenate([conv64_out, up64], axis = -1)
up64_out = conv64_up(merge64)


### 128x128 ###
deconv128 = upsample(filters = 64, kernel_size = 3, batch_norm = False, dropout = None)
conv128_up = conv_layer(filters = 5,kernel_size = 3, batch_norm = False, dropout = None)

# Operations
up128 = deconv128(up64_out)
merge128 = tf.keras.layers.concatenate([conv128_out, up128], axis = -1)
up128_out = conv128_up(merge128)



OUTPUTS = tf.keras.activations.softmax(up128_out)

model = tf.keras.Model(inputs=INPUTS, outputs=OUTPUTS, name='model')

# loss
loss = tf.keras.losses.CategoricalCrossentropy(from_logits=False)

# Compile
model.compile(optimizer='adam',loss=loss,metrics=[tf.keras.metrics.Precision()])

# Load weights
model.load_weights(weights_path)

# Freeze layers
for i in range(len(model.layers)):
    model.layers[i].trainable=False

model._make_predict_function()
############################################################################################




class SegmentationNN:

  def __init__(self):


    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/neural_network/segmentation/input_image",Image,self.callback)

    #self.nn_output_pub = rospy.Publisher("/neural_network/segmentation/output", nn_output, queue_size=1)

    self.counter = 0

  def callback(self,data):
    print("got image")
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      cv_image = cv2.resize(cv_image,(IMG_SIZE,IMG_SIZE))
    except CvBridgeError as e:
      print(e)


    # Time the exectuion 
    time_start = time.time()

    # Convert to RGB
    image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)

    # Expand dimension
    batch = np.zeros((1,IMG_SIZE,IMG_SIZE,3))
    batch[0,:,:,:] = image/255.0

    ##############################################################
    # predict and find boxes
    y_pred = model.predict(batch)
    print(y_pred.shape)

    rgb = mask2BGR(y_pred[0])
    #rgb = cv2.cvtColor(rgb, cv2.COLOR_RGB2BGR)

    print(time.time()-time_start)
    ##############################################################

    # show the output image
    cv2.imshow("Original", cv_image)
    cv2.imshow("segmented", rgb)
    #cv2.imshow("blades", y_pred[0,:,:,1])
    #cv2.imshow("tower", y_pred[0,:,:,2])
    cv2.waitKey(3)
    ############################################################

def mask2BGR(in_mask):
    rgb = np.zeros((IMG_SIZE,IMG_SIZE,3))
    
    mask = np.argmax(in_mask, axis=-1)

    rgb[:,:,1]+=np.equal(mask,np.ones(IMG_SIZE)*1) # blades (green)
    rgb[:,:,0]+=np.equal(mask,np.ones(IMG_SIZE)*4) # where blades meet (blue)

    # tower (yellow)
    rgb[:,:,2]+=np.equal(mask,np.ones(IMG_SIZE)*2)
    rgb[:,:,1]+=np.equal(mask,np.ones(IMG_SIZE)*2)

    # hub (purple)
    rgb[:,:,2]+=np.equal(mask,np.ones(IMG_SIZE)*3)
    rgb[:,:,0]+=np.equal(mask,np.ones(IMG_SIZE)*3)
    
    return rgb


def main(args):
    print("started segmentation_node")
    nn = SegmentationNN()
    rospy.init_node('segmentation_node', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
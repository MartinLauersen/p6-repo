#!/usr/bin/env python2
#from __future__ import print_functionimport roslib
import sys
import cv2
import rospy
import numpy as np
import time
import os
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from geometry_msgs.msg import TwistStamped
from sensor_msgs.msg import Imu
from std_msgs.msg import Bool
from p6_video_processing.msg import sensorOutput
from filterpy.kalman import KalmanFilter, ExtendedKalmanFilter
from tf.transformations import *


## export ROS_MASTER_URI=http://192.168.1.12:11311
## export ROS_IP=192.168.1.15
## rosrun p6_video_processing filtering.py



HEIGHT = 300                # Height of the model input images
WIDTH = HEIGHT                 # Width of the model input images
PX_X = 12.27e-6                # Pixel horizontal size 
PX_Y = 9.2e-6
FOCAL_LENGTH = 3.04e-3
BASELINE = 0.3
MAX_RANGE = 800
NANO = 1.0e-10
DTF = 0.5
NN_VAR_X = 3.0
NN_VAR_Y = 9.4
#ACC_COV = np.ones((3,3))*NANO
ACC_COV = np.array([[0.3,-0.2,-0.16],
                    [-0.2,0.34,0.08],
                    [-0.16,0.08,0.37]])
GPS_VAR = 0.03

w = 0.0625
wz = 0.03125
C = 2**12#2**14
                    # Initial Standard Deviations [pixel(x,y), Distance, distance, velocities, acceleration]
INIT_STD = np.array([WIDTH/6.0, MAX_RANGE/3.0, (BASELINE-(MAX_RANGE*WIDTH)/FOCAL_LENGTH)/6.0, 10/3.0, 4/3.0])# Convert time stamp to seconds

def fullFilter(x, P):
    kf = ExtendedKalmanFilter(dim_x=12, dim_z=9)
    kf.x = np.array([x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10], x[11]])
    kf.F = np.array([[1.0, 0.0, -DTF, -0.5*DTF**2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 1.0, DTF, 0.5*DTF**2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 1.0, DTF, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, -DTF, -0.5*DTF**2, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, DTF, 0.5*DTF**2, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, DTF, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, -DTF, -0.5*DTF**2],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, DTF, 0.5*DTF**2],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, DTF],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]])
    kf.P[:] = P
    q = np.array([[C*(DTF**4)/4,(DTF**4)/4,(DTF**3)/2,(DTF**2)/2],
                  [(DTF**4)/4,(DTF**4)/4,(DTF**3)/2,(DTF**2)/2],
                  [(DTF**3)/2,(DTF**3)/2,DTF**2    ,DTF],
                  [(DTF**2)/2,(DTF**2)/2,DTF       ,1]])
    o = np.zeros((4,4))
    kf.Q = np.block([[q*w,o,o],[o,q*w,o],[o,o,q*wz]])
    return kf

def hFull(x):
    zu = (x[4]-x[5])*FOCAL_LENGTH/(PX_X*(x[0]-x[1]))
    zv = (x[8]-x[9])*FOCAL_LENGTH/(PX_Y*(x[0]-x[1]))
    zd = x[0]-x[1]
    zgx = x[1]
    zgy = x[5]
    zgz = x[9]
    zax = x[3]
    zay = x[7]
    zaz = x[11]
    return np.array([zu, zv, zd, zgx, zgy, zgz, zax, zay, zaz])

def Hfull(x):
    dx, dy, dz = x[0]-x[1], x[4]-x[5], x[8]-x[9]
    jacob = np.array([[FOCAL_LENGTH*(-dy)/(PX_X*(dx**2)), FOCAL_LENGTH*(dy)/(PX_X*(dx**2)),0,0, FOCAL_LENGTH/(PX_X*(dx)), -FOCAL_LENGTH/(PX_X*(dx)),0,0,0,0,0,0],
                      [FOCAL_LENGTH*(-dz)/(PX_Y*(dx**2)), FOCAL_LENGTH*(dz)/(PX_Y*(dx**2)),0,0,0,0,0,0, FOCAL_LENGTH/(PX_Y*(dx)), -FOCAL_LENGTH/(PX_Y*(dx)),0,0],
                      [1.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                      [0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                      [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                      [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
                      [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                      [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
                      [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]])
    return jacob

def Dist_Var(d):
    std = 0.0002102*d**2 - 0.0583*d + 6.181
    return std**2

def h(x):
    zx = x[1]*FOCAL_LENGTH/(PX_X*x[0])
    zy = x[2]*FOCAL_LENGTH/(PX_Y*x[0])
    zd = x[0]
    return np.array([zx, zy, zd])

def Hj(x):
    jacob = np.array([[-x[1]*FOCAL_LENGTH/(PX_X*x[0]**2), FOCAL_LENGTH/(PX_X*x[0]), 0.0],
                    [-x[2]*FOCAL_LENGTH/(PX_Y*x[0]**2), 0.0, FOCAL_LENGTH/(PX_Y*x[0])],
                    [1.0, 0.0, 0.0]])
    return jacob



# Construct filter object for NN measurements
def nnFilter(x, P):
    kf = ExtendedKalmanFilter(dim_x=3, dim_z=3)
    kf.x = np.array([x[0], x[1], x[2]])
    kf.F = np.array([[1,0,0],[0,1,0],[0,0,1]])
    kf.P[:] = P
    kf.Q = 0.0
    
    return kf


class image_converter:
    
    def __init__(self):
        # Subscribers to sensors
        self.sensor_sub = rospy.Subscriber("/drone/sensors/turbine_sensor", sensorOutput, self.nnOutputCB)
        self.go_sub = rospy.Subscriber("/drone/controller/go", Bool, self.go)
        self.setpoint_pub = rospy.Publisher("/drone/controller/setpoints",TwistStamped,queue_size=1)  
        self.point_pub = rospy.Publisher("/drone/controller/point",Point,queue_size=1)            
        self.foo_pub = rospy.Publisher("/drone/controller/raw_distance",Point,queue_size=1)              
        self.foo2_pub = rospy.Publisher("/drone/controller/estimated_distance",Point,queue_size=1)        
        

        # Initial state of filter
        x0 = np.array([0.5*MAX_RANGE, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])     
        # Variances
        P = np.diag([INIT_STD[1]**2, INIT_STD[1]**2, INIT_STD[3]**2, INIT_STD[4]**2, INIT_STD[1]**2, INIT_STD[1]**2, INIT_STD[3]**2, INIT_STD[4]**2, INIT_STD[1]**2, INIT_STD[1]**2, INIT_STD[3]**2, INIT_STD[4]**2])       
        
        # Class variables                        
        
        self.points = Point()
        self.foo = Point()
        self.foo2 = Point()
        self.setpoints = TwistStamped()

        self.kf = fullFilter(x0,P)
        self.z = np.empty(9)
        self.R = np.diag(np.array([NN_VAR_X,NN_VAR_Y,INIT_STD[1]**2,GPS_VAR,GPS_VAR,GPS_VAR,0,0,0]))
        self.R[6:9,6:9] = ACC_COV

        self.b_start = False
        self.b_go = False
        self.error = np.array([0.0,0.0,0.0])
        self.count = 0
        
        print("Ready")    

    def go(self,data):
        self.b_go = True

    # This function is executed when the nn sensor publishes reading  
    def nnOutputCB(self,data):
        if self.b_go:
            print("")
            if len(data.distance):
                print("Got turbine")
                self.z[0] = WIDTH/2 - data.x[0]
                self.R[0,0] = NN_VAR_X 
                self.z[1] = WIDTH/2 - data.y[0]
                self.R[1,1] = NN_VAR_Y  
                if data.class_name[0] == "shaft":
                    self.z[1] += 0.5*data.h[0]
                if data.class_name[0] == "wings":
                    self.R[1,1] += 0.5*data.h[0]/NN_VAR_Y
                if data.distance[0] > -1.0:
                    self.b_start = True
                    print("Got distance")
                    self.z[2] = data.distance[0]
                    self.R[2,2] = data.variance[0]
                    self.foo.x = data.distance[0]
                    self.foo.y = data.distance[0]*PX_X*self.z[0]/FOCAL_LENGTH
                    self.foo.z = data.distance[0]*PX_Y*self.z[1]/FOCAL_LENGTH
                    print(data.distance[0])
                else:
                    print("No distance")
                    self.R[2,2] += self.kf.P[0,0]-self.kf.P[1,1]       
                print(data.class_name[0])
            else:
                print("No detection")
                self.R[0,0] *= 1.5
                self.R[1,1] *= 1.5
                self.R[2,2] += self.kf.P[0,0]-self.kf.P[1,1]
            self.z[3],self.z[4],self.z[5] = data.gps.pose.pose.position.x, data.gps.pose.pose.position.y, data.gps.pose.pose.position.z
            self.z[3] += np.random.normal()*GPS_VAR
            self.z[4] += np.random.normal()*GPS_VAR
            self.z[5] += np.random.normal()*GPS_VAR
            
            self.z[6],self.z[7],self.z[8] = data.imu.linear_acceleration.x, data.imu.linear_acceleration.y, data.imu.linear_acceleration.z
            noise = np.array([np.random.normal(),np.random.normal(),np.random.normal()])
            acc_noise = ACC_COV.dot(noise)
            self.z[6] += acc_noise[0]
            self.z[7] += acc_noise[1]
            self.z[8] += acc_noise[2]

            if self.b_start:
                self.kf.predict()

                self.points.x = self.kf.x[0]                -500#self.kf.x[1]
                self.points.y = self.kf.x[4] + BASELINE/2   -100#self.kf.x[5]
                self.points.z = self.kf.x[8]                -63#self.kf.x[9]

                self.foo2.x = self.kf.x[0]                -self.kf.x[1]
                self.foo2.y = self.kf.x[4] + BASELINE/2   -self.kf.x[5]
                self.foo2.z = self.kf.x[8]                -self.kf.x[9]
                
                self.kf.update(self.z,Hfull,hFull,self.R)
                self.kf.predict()

                self.setpoints.twist.linear.x = self.kf.x[0]               # -500
                self.setpoints.twist.linear.y = self.kf.x[4] + BASELINE/2  # -100
                self.setpoints.twist.linear.z = self.kf.x[8]               # -63
                #print(self.setpoints.twist.linear)
                self.setpoints.twist.linear.x -= 50
                self.setpoint_pub.publish(self.setpoints)

                self.point_pub.publish(self.points)
                self.foo_pub.publish(self.foo)
                self.foo2_pub.publish(self.foo2)


def main(args):
    print("started filtering_node")
    ic = image_converter()
    rospy.init_node('filtering_node', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
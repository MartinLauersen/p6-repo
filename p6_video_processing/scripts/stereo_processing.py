#!/usr/bin/env python2
#from __future__ import print_function

import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import time
import os

import math as m

from p6_video_processing.msg import nnOutput
from p6_video_processing.msg import sensorOutput

from sensor_msgs.msg import CameraInfo

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry


NN_INPUT_SIZE = (300,300)

BASELINE = 0.3

MAX_DISPARITY = 15


class image_converter:

    def __init__(self):

        self.nn_output_sub = rospy.Subscriber("/neural_network/detection/output", nnOutput, self.nnOutputCB)

        # Sub to imu ang ground truth
        self.imu_sub = rospy.Subscriber("/drone/sensors/imu", Imu, self.imuCB)
        self.gps_sub = rospy.Subscriber("/drone/sensors/ground_truth", Odometry, self.gpsCB)


        self.sensor_output_pub = rospy.Publisher("/drone/sensors/turbine_sensor",sensorOutput,queue_size=1)

        self.nn_image_pub = rospy.Publisher("/neural_network/detection/input_image",Image,queue_size=1)


        # Subscribe to both camera images
        self.bridge = CvBridge()
        self.image_left_sub = rospy.Subscriber("/drone/camera/left/image_raw",Image,self.imageLeftCB)
        self.image_right_sub = rospy.Subscriber("/drone/camera/right/image_raw",Image,self.imageRightCB)
        self.camera_info_left = rospy.Subscriber("/drone/camera/left/camera_info",CameraInfo,self.cameraInfoCB)
        
        self.b_image_left = False
        self.b_image_right = False

        self.focal_len = None
        self.image_timestamp = None

        self.disparity_prev = 0

        # Used to align data
        self.imu_img_time = Imu()
        self.imu_last = Imu()
        self.gps_img_time = Odometry()
        self.gps_last = Odometry()

    def cameraInfoCB(self,data):
        # save the focal len 
        self.focal_len = data.P[0]

    def imuCB(self,data):
        self.imu_last = data
    
    def gpsCB(self,data):
        self.gps_last = data

    def imageLeftCB(self,data):
        try:
            self.image_left = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        self.b_image_left = True
        self.image_timestamp = data.header.stamp

        self.imu_img_time = self.imu_last
        self.gps_img_time = self.gps_last


        self.nn_image_pub.publish(data)

    def imageRightCB(self,data):
        try:
            self.image_right = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        self.b_image_right = True

    def subPixInterpolateNEW(self, res, max_loc):

        # 
        print(max_loc)
        print(max_loc[0])

        x0 = -2
        x1 = -1
        x2 = 0
        x3 = 1
        x4 = 2

        v0 = res[0,max_loc[0]-2]
        v1 = res[0,max_loc[0]-1]
        v2 = res[0,max_loc[0]]
        v3 = res[0,max_loc[0]+1]
        v4 = res[0,max_loc[0]+2]

        A = np.array([[x0**4,x0**3,x0**2, x0 ,1],
                      [x1**4,x1**3,x1**2, x1, 1],
                      [x2**4,x2**3,x2**2, x2, 1],
                      [x3**4,x3**3,x3**2, x3, 1],
                      [x4**4,x4**3,x4**2, x4, 1]
        ])

        b = np.array([[v0],[v1],[v2],[v3], [v4]])
        abc = np.linalg.solve(A, b)

        # find where the derivative of x is 0 (max position)
        fx_max = -abc[1]/(2*abc[0])
        print("max {}".format(fx_max))
        print("vals: ")
        print(res)

        return max_loc[0] + fx_max

    def subPixInterpolate(self, res, max_loc):

        # 
        print(max_loc)
        print(max_loc[0])

        x1 = -1
        x2 = 0
        x3 = 1

        v1 = res[0,max_loc[0]-1]
        v2 = res[0,max_loc[0]]
        v3 = res[0,max_loc[0]+1]
        print(v1)

        A = np.array([[x1**2, x1 ,1],
                      [x2**2, x2, 1],
                      [x3**2, x3, 1]])

        b = np.array([[v1],[v2],[v3]])
        abc = np.linalg.solve(A, b)

        # find where the derivative of x is 0 (max position)
        fx_max = -abc[1]/(2*abc[0])
        print("max {}".format(fx_max))
        print("vals: ")
        print(res)

        return max_loc[0] + fx_max

    def calculateDistance(self, box):
        xs = int(box[0])
        ys = int(box[1])
        xe = int(box[2])
        ye = int(box[3])

        # Ensure that coordinates are in the valid range

        # Match 
        #tic = time.time()
        #res = cv2.matchTemplate(self.image_right[int(ys):int(ye), 
        #                        int(xs)-MAX_DISPARITY:int(xe)], self.image_left[int(ys):int(ye), 
        #                        int(xs):int(xe)], cv2.TM_CCOEFF_NORMED)
        #min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        #print("left image turbine top left {}, {}".format(int(xs),int(ys)))
        #print("min_val: {}, max_val: {}, min_loc: {}, max_loc: {}".format(min_val, max_val, min_loc, max_loc))
        #print("Template match RGB {}".format(time.time()-tic))

        # Convert to grayscale and match template
        #tic = time.time()


        # Check if xs-MAX_DISPARITY reaches outside of the image
        # If so clip the template so it fits
        edge_distance = int(xs) - MAX_DISPARITY

        xe -= 2 # check if it fixes the problem

        if edge_distance < 0:
            xs -= edge_distance

        gray_r = cv2.cvtColor(self.image_right[int(ys):int(ye),int(xs)-MAX_DISPARITY:int(xe+2)], cv2.COLOR_BGR2GRAY)
        gray_l = cv2.cvtColor(self.image_left[int(ys):int(ye), int(xs):int(xe)], cv2.COLOR_BGR2GRAY)

        res_gray = cv2.matchTemplate(gray_r, gray_l, cv2.TM_CCOEFF_NORMED)
        min_val_gray, max_val_gray, min_loc_gray, max_loc_gray = cv2.minMaxLoc(res_gray)

        max_loc_gray = self.subPixInterpolate(res_gray, max_loc_gray)

        ## show the pic that martin wants
        #for_martin = cv2.matchTemplate(gray_r,cv2.cvtColor(self.image_left, cv2.COLOR_BGR2GRAY),cv2.TM_CCOEFF_NORMED)
        #cv2.imshow("Matching", for_martin)
        #cv2.waitKey(1)


        # Calculate the distance.
        disparity = MAX_DISPARITY - max_loc_gray[0]
        if disparity > 0:
            distance = self.focal_len * BASELINE / disparity

            """
            #max_deviation = distance/2
            try:
                max_deviation = (distance**2)/(self.focal_len*BASELINE-distance)
            except:
                max_deviation = distance/2

            variance = (max_deviation/3)**2

            if self.disparity_prev != disparity:
                variance /= 3

            self.disparity_prev = disparity
            """
            av = 0.5001
            bv = 0.005573
            variance = (av*m.exp(bv*distance))**2

            # add offset
            #ao = 0.8544
            #bo = 0.004759
            #distance += ao*m.exp(bo*distance)
            
        else:
            # distance unknown 
            distance = -1
            variance = 999999999

        print(distance)

        return distance, variance


    def nnOutputCB(self,data):

        # 
        if self.b_image_right:
            self.b_image_right = False

            num_objects = len(data.class_name)

            # 
            msg = sensorOutput()
            msg.stamp = self.image_timestamp

            hub_found,wings_found,shaft_found, housing_found = False, False, False, False
            hub_index, wings_index, shaft_index, housing_index = [], [], [], []
            

            # check if hub detected
            for i in range(num_objects):
                if data.class_name[i] == 'hub':
                    hub_found = True
                    hub_index.append(i)
                if data.class_name[i] == 'wings':
                    wings_found = True
                    wings_index.append(i)
                if data.class_name[i] == 'shaft':
                    shaft_found = True
                    shaft_index.append(i)
                if data.class_name[i] == 'housing':
                    housing_found = True
                    housing_index.append(i)

            if hub_found == True:
                data_index = hub_index[0]
            elif housing_found == True:
                data_index = housing_index[0]
            elif shaft_found == True:
                data_index = shaft_index[0]
            elif wings_found == True:
                data_index = wings_index[0]
            else:
                msg.imu = self.imu_img_time
                msg.gps.pose = self.gps_img_time.pose
                self.sensor_output_pub.publish(msg)
                return

            # scale factor between original image and nn input
            sx = self.image_left.shape[1]/float(NN_INPUT_SIZE[0])
            sy = self.image_left.shape[0]/float(NN_INPUT_SIZE[1])

            xs = data.x_start[data_index] * sx
            ys = data.y_start[data_index] *sy
            xe = data.x_end[data_index] * sx
            ye = data.y_end[data_index] *sy

            # Verify that the coordinates are within pixel range
            xs = max([0, xs])
            ys = max([0, ys])
            xe = min([self.image_left.shape[1], xe])
            ye = min([self.image_left.shape[0],ye ])


            box = [xs, ys, xe, ye]
            distance, variance = self.calculateDistance(box)
        
            # Pack msg
            msg.class_name.append(data.class_name[data_index])
            msg.confidence.append(data.confidence[data_index])
            msg.x.append((data.x_start[data_index] + data.x_end[data_index])*0.5)
            msg.y.append((data.y_start[data_index] + data.y_end[data_index])*0.5)
            msg.w.append(data.x_end[data_index] - data.x_start[data_index])
            msg.h.append(data.y_end[data_index] - data.y_start[data_index])

            msg.distance.append(distance)
            msg.variance.append(variance)

            
            msg.imu = self.imu_img_time
            msg.gps.pose = self.gps_img_time.pose

            self.sensor_output_pub.publish(msg)

"""
            ### Show image
            tic = time.time()
            window_size = 5
            min_disp = 32
            num_disp = 112-min_disp

            print("aa")
            image_left = cv2.resize(self.image_left,(1920,1080))
            image_right = cv2.resize(self.image_right,(1920,1080))

            image_left = cv2.cvtColor(image_left, cv2.COLOR_BGR2GRAY)
            image_right = cv2.cvtColor(image_right, cv2.COLOR_BGR2GRAY)
            print("bb")
            # compute disparity

            print("cc")
            stereo = cv2.StereoBM_create(numDisparities=32, blockSize=15)
            disparity = stereo.compute(image_left,image_right)
            print(time.time()-tic)
            mina = disparity.min()
            maxa = disparity.max()
            disparity = np.uint8(6400 * (disparity - mina) / (maxa - mina))
            cv2.imshow("Stereo images", disparity)
            cv2.waitKey(3)
            """

def main(args):
    print("started stereo_processing_node")
    ic = image_converter()
    rospy.init_node('video_stereo_node', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)










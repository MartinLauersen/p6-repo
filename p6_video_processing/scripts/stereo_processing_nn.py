#!/usr/bin/env python2
#from __future__ import print_function

import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import time
import os

from p6_video_processing.msg import nnOutput
from p6_video_processing.msg import sensorOutput

from sensor_msgs.msg import CameraInfo

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry


NN_INPUT_SIZE = (300,300)

BASELINE = 0.3

MAX_DISPARITY = 15


class image_converter:

    def __init__(self):

        self.nn_output_sub = rospy.Subscriber("/neural_network/detection/output", nnOutput, self.nnOutputCB)

        # Sub to imu ang ground truth
        self.imu_sub = rospy.Subscriber("/drone/sensors/imu", Imu, self.imuCB)
        self.gps_sub = rospy.Subscriber("/drone/sensors/ground_truth", Odometry, self.gpsCB)


        self.sensor_output_pub = rospy.Publisher("/drone/sensors/turbine_sensor",sensorOutput,queue_size=1)

        self.nn_image_pub = rospy.Publisher("/neural_network/detection/input_image",Image,queue_size=1)


        # Subscribe to both camera images
        self.bridge = CvBridge()
        self.image_left_sub = rospy.Subscriber("/drone/camera/left/image_raw",Image,self.imageLeftCB)
        self.image_right_sub = rospy.Subscriber("/drone/camera/right/image_raw",Image,self.imageRightCB)
        self.camera_info_left = rospy.Subscriber("/drone/camera/left/camera_info",CameraInfo,self.cameraInfoCB)
        
        self.b_image_left = False
        self.b_image_right = False

        self.focal_len = None
        self.image_timestamp = None

        self.disparity_prev = 0

        self.left_center = 0
        self.right_center = 0

        # Used to align data
        self.imu_img_time = Imu()
        self.imu_last = Imu()
        self.gps_img_time = Odometry()
        self.gps_last = Odometry()

    def cameraInfoCB(self,data):
        # save the focal len 
        self.focal_len = data.P[0]

    def imuCB(self,data):
        self.imu_last = data
    
    def gpsCB(self,data):
        self.gps_last = data

    def imageLeftCB(self,data):
        try:
            self.image_left = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        self.b_image_left = True
        self.image_timestamp = data.header.stamp

        self.imu_img_time = self.imu_last
        self.gps_img_time = self.gps_last


        self.nn_image_pub.publish(data)

    def imageRightCB(self,data):
        try:
            self.image_right = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        self.nn_image_pub.publish(data)

        self.b_image_right = True

    def calculateDistance(self, box):
        xs = int(box[0])
        ys = int(box[1])
        xe = int(box[2])
        ye = int(box[3])

        # Ensure that coordinates are in the valid range

        print(xs,ys)

        # Match 
        #tic = time.time()
        #res = cv2.matchTemplate(self.image_right[int(ys):int(ye), 
        #                        int(xs)-MAX_DISPARITY:int(xe)], self.image_left[int(ys):int(ye), 
        #                        int(xs):int(xe)], cv2.TM_CCOEFF_NORMED)
        #min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        #print("left image turbine top left {}, {}".format(int(xs),int(ys)))
        #print("min_val: {}, max_val: {}, min_loc: {}, max_loc: {}".format(min_val, max_val, min_loc, max_loc))
        #print("Template match RGB {}".format(time.time()-tic))

        # Convert to grayscale and match template
        tic = time.time()
        gray_r = cv2.cvtColor(self.image_right[int(ys):int(ye),int(xs)-MAX_DISPARITY:int(xe)], cv2.COLOR_BGR2GRAY)
        gray_l = cv2.cvtColor(self.image_left[int(ys):int(ye), int(xs):int(xe)], cv2.COLOR_BGR2GRAY)

        res_gray = cv2.matchTemplate(gray_r, gray_l, cv2.TM_CCOEFF_NORMED)
        min_val_gray, max_val_gray, min_loc_gray, max_loc_gray = cv2.minMaxLoc(res_gray)



        # Calculate the distance.
        disparity = MAX_DISPARITY - max_loc_gray[0]
        if disparity > 0:
            distance = self.focal_len * BASELINE / disparity

            #max_deviation = distance/2
            try:
                max_deviation = (distance**2)/(self.focal_len*BASELINE-distance)
            except:
                max_deviation = distance/2

            variance = (max_deviation/3)**2

            if self.disparity_prev != disparity:
                variance /= 3

            self.disparity_prev = disparity
            
        else:
            # distance unknown 
            distance = -1
            variance = 999999999

        return distance, variance


    def nnOutputCB(self,data):

        # 

        ####
        if self.b_image_left == True:
            self.b_image_left = False

            for i in range(len(data.class_name)):
                if data.class_name[i] == 'wings':
                    cx = (data.x_end[i] + data.x_start[i])*0.5
                    self.left_center = cx

        elif self.b_image_right == True:
            self.b_image_right = False

            for i in range(len(data.class_name)):
                if data.class_name[i] == 'wings':
                    cx = (data.x_end[i] + data.x_start[i])*0.5
                    self.right_center = cx

            msg = sensorOutput()
            disparity = abs(self.right_center - self.left_center)
            if disparity > 0.1:
                distance = 259*BASELINE / disparity
                print("Distance: {}".format( distance))
                msg.distance.append(distance)
            

            
            msg.gps = self.gps_img_time
            self.sensor_output_pub.publish(msg)



def main(args):
    print("started stereo_processing_node")
    ic = image_converter()
    rospy.init_node('video_stereo_node', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)










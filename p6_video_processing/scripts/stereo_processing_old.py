#!/usr/bin/env python2
#from __future__ import print_function

import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import time
import os

from p6_video_processing.msg import nnOutput
from p6_video_processing.msg import sensorOutput

from sensor_msgs.msg import CameraInfo


NN_INPUT_SIZE = (300,300)

BASELINE = 0.3


class image_converter:

    def __init__(self):

        self.nn_output_sub = rospy.Subscriber("/neural_network/detection/output", nnOutput, self.nnOutputCB)

        self.sensor_output_pub = rospy.Publisher("/drone/sensors/turbine_sensor",sensorOutput,queue_size=1)

        self.nn_image_pub = rospy.Publisher("/neural_network/detection/input_image",Image,queue_size=1)


        # Subscribe to both camera images
        self.bridge = CvBridge()
        self.image_left_sub = rospy.Subscriber("/drone/camera/left/image_raw",Image,self.imageLeftCB)
        self.image_right_sub = rospy.Subscriber("/drone/camera/right/image_raw",Image,self.imageRightCB)
        self.camera_info_left = rospy.Subscriber("/drone/camera/left/camera_info",CameraInfo,self.cameraInfoCB)
        
        self.b_image_left = False
        self.b_image_right = False

        self.focal_len = None
        self.image_timestamp = None

    def cameraInfoCB(self,data):
        # save the focal len 
        self.focal_len = data.P[0]

    def imageLeftCB(self,data):
        try:
            self.image_left = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        self.b_image_left = True
        self.image_timestamp = data.header.stamp

        self.nn_image_pub.publish(data)

    def imageRightCB(self,data):
        try:
            self.image_right = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        self.b_image_right = True

    def calculateDistance(self, box):
        xs = int(box[0])
        ys = int(box[1])
        xe = int(box[2])
        ye = int(box[3])

        # Match 
        tic = time.time()
        res = cv2.matchTemplate(self.image_right[int(ys):int(ye), 
                                int(xs)-10:int(xe)], self.image_left[int(ys):int(ye), 
                                int(xs):int(xe)], cv2.TM_CCOEFF_NORMED)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        #print("left image turbine top left {}, {}".format(int(xs),int(ys)))
        #print("min_val: {}, max_val: {}, min_loc: {}, max_loc: {}".format(min_val, max_val, min_loc, max_loc))
        print(time.time()-tic)

        #cv2.rectangle(self.image_left, (int(xs), int(ys)), (int(xe), int(ye)), (0 , 0, 255), 1)
        #width = int(xe-xs)
        #height = int(ye-ys)
        #cv2.rectangle(self.image_right, (max_loc), (max_loc[0] + width, max_loc[1] + height), (0 , 0, 255), 1)

        # Calculate the distance.
        disparity = 10 - max_loc[0]
        if disparity > 0:
            distance = self.focal_len * BASELINE / disparity

            #max_deviation = distance/2
            try:
                max_deviation = (distance**2)/(self.focal_len*BASELINE-distance)
            except:
                max_deviation = distance/2

            variance = (max_deviation/3)**2
            
        else:
            # distance unknown 
            distance = -1
            variance = 999999999

        return distance, variance


    def nnOutputCB(self,data):

        # 
        if self.b_image_right:
            self.b_image_right = False

            num_objects = len(data.class_name)

            # 
            msg = sensorOutput()
            msg.stamp = self.image_timestamp

            hub_found,wings_found = False, False
            hub_index, wings_index = [], []
            

            # check if hub detected
            for i in range(num_objects):
                if data.class_name[i] == 'hub':
                    hub_found = True
                    hub_index.append(i)
                if data.class_name[i] == 'wings'
                    wings_found = True
                    wings_index.append(i)

            for i in range(num_objects):


                if data.class_name[i] == 'wings':

                    # scale factor between original image and nn input
                    sx = self.image_left.shape[1]/float(NN_INPUT_SIZE[0])
                    sy = self.image_left.shape[0]/float(NN_INPUT_SIZE[1])

                    xs = data.x_start[i] * sx
                    ys = data.y_start[i] *sy
                    xe = data.x_end[i] * sx
                    ye = data.y_end[i] *sy

                    box = [xs, ys, xe, ye]
                    distance, variance = self.calculateDistance(box)
                
                    # Pack msg
                    msg.class_name.append(data.class_name[i])
                    msg.confidence.append(data.confidence[i])
                    msg.x.append((data.x_start[i] + data.x_end[i])*0.5)
                    msg.y.append((data.y_start[i] + data.y_end[i])*0.5)
                    msg.w.append(data.x_end[i] - data.x_start[i])
                    msg.h.append(data.y_end[i] - data.y_start[i])

                    msg.distance.append(distance)
                    msg.variance.append(variance)

            self.sensor_output_pub.publish(msg)


def main(args):
    print("started stereo_processing_node")
    ic = image_converter()
    rospy.init_node('video_stereo_node', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)










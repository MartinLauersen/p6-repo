#!/usr/bin/env python2
#from __future__ import print_function

import sys


import rospy

from p6_video_processing.msg import nnOutput
from p6_video_processing.msg import sensorOutput




class Test:


    def __init__(self):

        # Subscribes to the output of the neural network
        self.nn_output_sub = rospy.Subscriber("/drone/sensors/turbine_sensor", sensorOutput, self.nnOutputCB)

        

    def nnOutputCB(self,data):

        print(data.imu)
        print(data.gps)
        








def main(args):
    print("test_node")
    test = Test()
    rospy.init_node('test_node', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")

if __name__ == '__main__':
    main(sys.argv)










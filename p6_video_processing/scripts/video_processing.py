#!/usr/bin/env python2
#from __future__ import print_function

import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import time
import os


from p6_video_processing.msg import nn_output

tracker_name = 'CSRT'

class image_converter:


  def __init__(self):


    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/drone/camera1/image_raw",Image,self.imageCB)

    self.nn_output_sub = rospy.Subscriber("/neural_network/output", nn_output, self.nnOutputCB)

    self.nn_image_pub = rospy.Publisher("/neural_network/input_image",Image,queue_size=1)

    # Image that was sent to nn for processing
    self.nn_image = None

    # Output of the neural network
    self.nn_output = None

    self.counter = 0

    self.tracker = None
    self.tracker_type = None
    self.track_flag = False
    self.track_contours =[]

  def nnOutputCB(self,data):

    # Save the neural network output
    self.nn_output = data

  def imageCB(self,data):
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      cv_image = cv2.resize(cv_image,(300,300))
    except CvBridgeError as e:
      print(e)


    # process one out of 30 images
    if self.counter < 45:
        self.counter += 1
    else:
      self.counter = 0

      # Send the picture to neural network
      self.nn_image_pub.publish(data)
      self.nn_image = cv_image
      self.nn_output = None

    # If nn_outout is none then just update the tracker
    if self.nn_output == None:
      
      #if self.track_flag == True:
      if self.tracker is not None:
        self.trackerUpdate(cv_image)


    # Check if there is a new neural network output
    else:
      
      num_objects = len(self.nn_output.class_name)

      # Draw boxes
      roi = [] # bounding box

      for i in range(num_objects):
        xs = int(self.nn_output.x_start[i])
        ys = int(self.nn_output.y_start[i])
        xe = int(self.nn_output.x_end[i])
        ye = int(self.nn_output.y_end[i])
        # label, confidence, x start , y start, x end, y end
        image = cv2.rectangle(self.nn_image, (xs,ys), (xe,ye), (0,255,0), 1) 
        conf_str = "Confidence {:.2f}".format(self.nn_output.confidence[i])
        #cv2.putText(self.nn_image, conf_str, xs, ys-10, cv2.FONT_HERSHEY_SIMPLEX, 0.4, (36,255,12), 1)

        # Reinitialize the tracker
        
        roi.append([xs, ys, xe - xs, ye - ys])
      self.tracker = None
      self.track_flag = False
      self.track_contours = []
      if len(roi)>0:
        self.trackerUpdate(self.nn_image, roi)
        self.trackerUpdate(cv_image)

      print(self.nn_output)

      self.nn_output = None

        
        # Plot boxes

    if len(self.track_contours) > 0:
      # loop over the track contours
      for (x,y,w,h) in self.track_contours:
        
        # Convert to integers
        x=int(x)
        y=int(y)
        w=int(w)
        h=int(h)
          
        # compute the bounding box for the contour, draw it on the frame,
        # and update the text
        cv2.rectangle(cv_image, (x, y), (x + w, y + h), (0 , 0, 255), 1)


    #cv2.imshow("Object detection", self.nn_image)


    cv2.imshow("Tracking", cv_image)
    cv2.waitKey(3)

  def setupTracker(self, tracker_name):
    # Select tracking algorithm
    if tracker_name == 'MOSSE':
        self.tracker_type = cv2.TrackerMOSSE_create()
    elif tracker_name == 'KCF':
        self.tracker_type = cv2.TrackerKCF_create()
    elif tracker_name == 'BOOSTING':
        self.tracker_type = cv2.TrackerBoosting_create()
    elif tracker_name == 'MIL':
        self.tracker_type = cv2.TrackerMIL_create()
    elif tracker_name == 'TLD':
        self.tracker_type = cv2.TrackerTLD_create()
    elif tracker_name == 'MEDIANFLOW':
        self.tracker_type = cv2.TrackerMedianFlow_create()
    elif tracker_name == 'GOTURN':
        self.tracker_type = cv2.TrackerGOTURN_create()        
    elif tracker_name == "CSRT":
        self.tracker_type = cv2.TrackerCSRT_create()
    else:
        print("Invalid tracker type")
        sys.exit()
    # Create MultiTracker object
    self.tracker = cv2.MultiTracker_create()

  def trackerUpdate(self, frame, roi = []):
      """ Tracks objects between frames. If region of interest (roi) is not
          passed then the function will just update the tracker
          
          Input: roi - list of bounding boxes (x, y, w, h) or nothing
          
          Output: Initiates a new tracker if a region of interest is given
                  or updates current tracker if there is no input
      """
      
      # add objects to the tracker
      if len(roi) != 0:
          # Initialize a new tracker
          self.setupTracker(tracker_name)
          
          # Add objects to the tracker
          for obj in roi:
              self.tracker.add(self.tracker_type, frame, tuple(obj))
              
      # Update frame
      self.track_flag, self.track_contours = self.tracker.update(frame)

def main(args):
    print("started video_processing_node")
    ic = image_converter()
    rospy.init_node('video_processing_node', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)









